FROM docker.io/debian:buster-slim

# Node.JS & PM2 base image for OpenShift Origin

ARG DO_UPGRADE=
ARG NODEJS_VERSION=14
ENV APP_ROOT=/opt/app-root \
    DEBIAN_FRONTEND=noninteractive \
    NAME="nodejs" \
    NPM_RUN=start \
    NPM_CONFIG_PREFIX=$HOME/.npm-global \
    PATH=$HOME/node_modules/.bin/:$HOME/.npm-global/bin/:$PATH \
    PM2_HOME=/opt/app-root/lib/pm2 \
    RTDIR=/opt/app-root/src \
    STI_SCRIPTS_PATH=/usr/libexec/s2i \
    YARN_URL=https://github.com/yarnpkg/yarn/releases/download \
    YARN_VERSION=1.22.15

ENV DESCRIPTION="Node.js $NODEJS_VERSION available as container is a base platform for \
building and running various Node.js $NODEJS_VERSION applications and frameworks. \
Node.js is a platform built on Chrome's JavaScript runtime for easily building \
fast, scalable network applications. Node.js uses an event-driven, non-blocking I/O model \
that makes it lightweight and efficient, perfect for data-intensive real-time applications \
that run across distributed devices." \
    SUMMARY="Platform for building and running Node.JS $NODEJS_VERSION apps"

LABEL summary="$SUMMARY" \
      description="$DESCRIPTION" \
      io.k8s.description="$DESCRIPTION" \
      io.k8s.display-name="Node.js $NODEJS_VERSION" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder,$NAME,$NAME$NODEJS_VERSION" \
      io.openshift.s2i.scripts-url="image://$STI_SCRIPTS_PATH" \
      io.s2i.scripts-url="image://$STI_SCRIPTS_PATH" \
      com.redhat.deployments-dir="$APP_ROOT/src" \
      name="opsperator/$NAME-$NODEJS_VERSION-debian10" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-nodejs" \
      usage="s2i build <SOURCE-REPOSITORY> opsperator/nodejs:latest <APP-NAME>" \
      version="$NODEJS_VERSION"

RUN set -x \
    && echo "# Install Dumb-init" \
    && apt-get update \
    && apt-get -y install dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && if test "$DEBUG"; then \
	echo "# Install Debugging Tools" \
	&& apt-get -y install vim; \
    fi \
    && echo "# Install NodeJS Dependencies" \
    && apt-get install --no-install-recommends --no-install-suggests -y \
	ldap-utils libcrypt-openssl-rsa-perl default-mysql-client mongo-tools \
	wget bsd-mailx curl git libnss-wrapper postgresql-client perl \
	openssl libssl-dev ca-certificates python-pip gcc make jq g++ bzip2 \
	libmime-tools-perl pkg-config \
    && curl -sL https://deb.nodesource.com/setup_$NODEJS_VERSION.x | bash - \
    && apt-get -y install nodejs \
    && pip install --upgrade s3cmd \
    && if test "$DB_CONNECTOR" = cassandra; then \
	pip install --upgrade cqlsh; \
    fi \
    && curl --location \
	"$YARN_URL/v$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz" \
	-o- | tar -C /opt -xzf - \
    && echo "# Cleaning up" \
    && apt-get -y remove --purge python-pip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/usr/local/share/doc /usr/local/share/man

COPY ./s2i/bin/ $STI_SCRIPTS_PATH
COPY ./root/ /
ENV PATH=/opt/yarn-v$YARN_VERSION/bin:$PATH
RUN set -x \
    && if test "$HTTP_PROXY"; then \
	( \
	    echo registry=http://registry.npmjs.org/ \
	    && echo http-proxy=$HTTP_PROXY \
	    && echo proxy=$HTTP_PROXY \
	    && echo strict-ssl=false \
	) >$HOME/.npmrc; \
    fi \
    && npm cache clean --force \
    && npm install -g node-gyp pm2 webpack-cli \
    && npm cache clean --force \
    && rm -fr /tmp/npm* $HOME/.npm $HOME/.npmrc \
    && for dir in $PM2_HOME $APP_ROOT $HOME/.npm $RTDIR; \
	do \
	    mkdir -p $dir 2>/dev/null \
	    && chown -R 1001:root "$dir" \
	    && chmod -R g=u "$dir"; \
	done \
    && for i in /etc/ssl /usr/local/share/ca-certificates /.npm-global; \
	do \
	    chown -R 1001:root "$i" \
	    && chmod -R g=u "$i"; \
	done \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
EXPOSE 8080
ENV HOME=$PM2_HOME
CMD $STI_SCRIPTS_PATH/usage
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
WORKDIR $RTDIR
