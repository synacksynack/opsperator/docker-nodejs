# k8s NodeJS

Generic NodeJS s2i base image, forked from
https://github.com/sclorg/s2i-nodejs-container

Build with:

```
$ make build
```

Build in OpenShift:

```
$ make ocbuild
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name  |    Description             | Default            |
| :---------------- | -------------------------- | ------------------ |
|  `NODE_ENV`       | NodeJS Environment         | `production`       |
|  `NPM_RUN`        | NodeJS Startup Command     | `run`              |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                       |
| :------------------ | --------------------------------- |
|  `/certs`           | App Certificate (optional)        |
